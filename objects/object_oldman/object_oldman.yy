{
    "id": "45634a5a-eda3-4bcc-881c-2e5f4205667e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_oldman",
    "eventList": [
        {
            "id": "b012cdfb-14ff-4dfb-9d64-c6427f03fc30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45634a5a-eda3-4bcc-881c-2e5f4205667e"
        },
        {
            "id": "0eabf20e-2ef2-4252-a9a7-690bbceafa04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1927248f-815f-4e41-b2bb-fbbf2b6395d7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "45634a5a-eda3-4bcc-881c-2e5f4205667e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1677208f-6937-4b32-9e10-e09e3dc0a986",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
    "visible": true
}