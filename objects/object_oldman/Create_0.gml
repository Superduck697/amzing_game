/// @DnDAction : YoYo Games.Paths.Start_Path
/// @DnDVersion : 1.1
/// @DnDHash : 47BFE770
/// @DnDArgument : "path" "path_1A"
/// @DnDArgument : "speed" "10"
/// @DnDArgument : "atend" "path_action_reverse"
/// @DnDSaveInfo : "path" "d81f21df-5e18-4330-8c4c-80b90eb99afa"
path_start(path_1A, 10, path_action_reverse, false);

/// @DnDAction : YoYo Games.Paths.Path_Position
/// @DnDVersion : 1
/// @DnDHash : 72279CE1
/// @DnDArgument : "position" "random(1)"
path_position = random(1);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 2177C3E9
/// @DnDArgument : "expr" "200"
/// @DnDArgument : "var" "hp"
hp = 200;