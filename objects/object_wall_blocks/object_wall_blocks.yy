{
    "id": "fefefcbc-ccb7-4c41-bf8f-5e25fadb0dcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall_blocks",
    "eventList": [
        {
            "id": "30435977-1653-4b10-b220-7edeedcebda2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fefefcbc-ccb7-4c41-bf8f-5e25fadb0dcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "09e43c50-0501-4842-9b76-f54f4b3450f0",
    "visible": true
}