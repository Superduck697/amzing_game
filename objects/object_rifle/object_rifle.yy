{
    "id": "cfc85cb5-7af6-4131-92b8-9f381629e602",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_rifle",
    "eventList": [
        {
            "id": "fdd33be6-4390-4fb5-9e91-cafd8f293614",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1805a338-a8ed-42f1-b217-3c947fbeebb8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cfc85cb5-7af6-4131-92b8-9f381629e602"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3202e9a0-2827-4e46-886b-1707d7855f45",
    "visible": true
}