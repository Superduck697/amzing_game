/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6915BE58
/// @DnDArgument : "var" "gun"
/// @DnDArgument : "value" "1"
if(gun == 1)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
	/// @DnDVersion : 1
	/// @DnDHash : 6DE629B8
	/// @DnDParent : 6915BE58
	/// @DnDArgument : "x" "mouse_x"
	/// @DnDArgument : "y" "mouse_y"
	direction = point_direction(x, y, mouse_x, mouse_y);

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Mouse_Pressed
	/// @DnDVersion : 1.1
	/// @DnDHash : 39D98278
	/// @DnDParent : 6915BE58
	var l39D98278_0;
	l39D98278_0 = mouse_check_button_pressed(mb_left);
	if (l39D98278_0)
	{
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 0D1DAAB3
		/// @DnDParent : 39D98278
		/// @DnDArgument : "xpos" "x"
		/// @DnDArgument : "ypos" "y"
		/// @DnDArgument : "objectid" "object_bullet"
		/// @DnDArgument : "layer" ""Instances_1""
		/// @DnDSaveInfo : "objectid" "1927248f-815f-4e41-b2bb-fbbf2b6395d7"
		instance_create_layer(x, y, "Instances_1", object_bullet);
	}
}