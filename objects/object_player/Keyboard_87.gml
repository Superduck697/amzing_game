/// @DnDAction : YoYo Games.Movement.Jump_To_Point
/// @DnDVersion : 1
/// @DnDHash : 6B4B5BC3
/// @DnDArgument : "x" "0"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "-7"
/// @DnDArgument : "y_relative" "1"
x += 0;
y += -7;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 0B97AC91
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 42DB9CA6
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_up"
/// @DnDSaveInfo : "spriteind" "4f96eca6-ac1c-443f-8a5a-fb54c2939a85"
sprite_index = sprite_player_up;
image_index = image_index;