/// @DnDAction : YoYo Games.Movement.Jump_To_Point
/// @DnDVersion : 1
/// @DnDHash : 46B9DADA
/// @DnDArgument : "x" "0"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "7"
/// @DnDArgument : "y_relative" "1"
x += 0;
y += 7;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 54D85876
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 6798CEBF
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_down"
/// @DnDSaveInfo : "spriteind" "39df3cc8-e979-4fb7-b8cc-90620ccfea09"
sprite_index = sprite_player_down;
image_index = image_index;