{
    "id": "1805a338-a8ed-42f1-b217-3c947fbeebb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "f9c85ce9-3b76-4c57-8120-6a3bec847656",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "76c4ac9b-70a8-4bdc-be01-628897b95e49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "6bd73dd4-6006-4633-83ae-ee48c89a2608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "37b5d57d-216d-41b1-9742-1d80c0d96fed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "dd5183d1-64b8-4b3e-9368-944afe19cacb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "39057f23-b27c-458b-860b-7015ad669426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "27a83164-bce3-42cb-a652-44e249deaa82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fefefcbc-ccb7-4c41-bf8f-5e25fadb0dcf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "a065e0ac-d9c8-4eab-a2bd-25e3ff0e0d40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bfea46f2-3b53-4df6-95a1-93d8da581cb9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "1593efb3-c61a-4449-9c3f-425d053124f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "8a697034-df6c-415f-9219-ee53ebca2065",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e6377c56-ff44-4e15-b4bd-c73f86745cac",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "e2c94320-564b-452a-9751-c0d456d27879",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "2fb92e8b-c762-4fb4-9bf6-b4f086d07059",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1677208f-6937-4b32-9e10-e09e3dc0a986",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "238d481a-d7c5-4cce-97aa-d20374d43503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "2aae02f5-1e3c-42c4-8172-7671e886fd43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cfc85cb5-7af6-4131-92b8-9f381629e602",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "9f6bd0de-0a4e-4b17-8934-dde5d37f41a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "d8eed2cb-13f8-4ccb-a271-00bd9542e886",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ec5b23d-152b-4825-bb71-fe6bb1b4ec02",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        },
        {
            "id": "1285815c-9fc8-40db-80f4-35288dbdadbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "1805a338-a8ed-42f1-b217-3c947fbeebb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
    "visible": true
}