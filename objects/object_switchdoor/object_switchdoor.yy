{
    "id": "2ec5b23d-152b-4825-bb71-fe6bb1b4ec02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_switchdoor",
    "eventList": [
        {
            "id": "ace9a58b-12ae-447c-92de-273345408900",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ec5b23d-152b-4825-bb71-fe6bb1b4ec02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6d0507fb-1011-4a22-a342-5575eb53a977",
    "visible": true
}