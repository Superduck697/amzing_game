{
    "id": "1927248f-815f-4e41-b2bb-fbbf2b6395d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "910abf82-f553-49d7-a38e-860c7b0920e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "1927248f-815f-4e41-b2bb-fbbf2b6395d7"
        },
        {
            "id": "401ceaad-d4f9-42a0-8596-aee1ab10ca1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fefefcbc-ccb7-4c41-bf8f-5e25fadb0dcf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1927248f-815f-4e41-b2bb-fbbf2b6395d7"
        },
        {
            "id": "80648cb4-54b3-4506-bd5c-0ff798b2ecb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1927248f-815f-4e41-b2bb-fbbf2b6395d7"
        },
        {
            "id": "5329d51f-9973-470c-937a-df51857e6da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "45634a5a-eda3-4bcc-881c-2e5f4205667e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1927248f-815f-4e41-b2bb-fbbf2b6395d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f62eaa46-121d-494f-8623-7f6090224915",
    "visible": true
}