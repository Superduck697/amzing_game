{
    "id": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_switch",
    "eventList": [
        {
            "id": "5083ae66-bb89-419a-a2d4-07abca5860dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d"
        },
        {
            "id": "ba47a553-1699-420e-a2b2-e1e5b803542f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d"
        },
        {
            "id": "10f63447-2172-4434-a500-e0f89d980603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d"
        },
        {
            "id": "abc7fb9b-a65a-443d-a31a-c1b4d9a9118d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d"
        },
        {
            "id": "5b61fbc9-9835-4db1-a0c0-ce370510adb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1805a338-a8ed-42f1-b217-3c947fbeebb8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a1370fc1-2eaf-4c00-b7b0-e54ea3b8f91d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0050696e-769f-46eb-8e8d-72d130586cd2",
    "visible": true
}