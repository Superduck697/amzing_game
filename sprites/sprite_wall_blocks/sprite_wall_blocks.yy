{
    "id": "09e43c50-0501-4842-9b76-f54f4b3450f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f40a2373-c88e-4d2e-b286-8933074780a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e43c50-0501-4842-9b76-f54f4b3450f0",
            "compositeImage": {
                "id": "e2c17cf1-c9d1-4a60-a53d-3b6258edf2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40a2373-c88e-4d2e-b286-8933074780a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4695f285-172c-4ea1-b0d9-841de8e722d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40a2373-c88e-4d2e-b286-8933074780a8",
                    "LayerId": "a0928bde-c9d8-416e-b86e-a8b4b9cc86d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a0928bde-c9d8-416e-b86e-a8b4b9cc86d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09e43c50-0501-4842-9b76-f54f4b3450f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}