{
    "id": "d5341eaf-dd25-45cc-9043-9be7ee2cadbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44f5e697-e22e-4960-ab47-4d6058755e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5341eaf-dd25-45cc-9043-9be7ee2cadbf",
            "compositeImage": {
                "id": "98407029-4387-4df9-910a-8dbf0be716f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44f5e697-e22e-4960-ab47-4d6058755e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a5b789-ef9a-452c-929a-6a9cb133e075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44f5e697-e22e-4960-ab47-4d6058755e79",
                    "LayerId": "0d8c23e5-a354-4a9a-afbe-e1258ea9d680"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d8c23e5-a354-4a9a-afbe-e1258ea9d680",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5341eaf-dd25-45cc-9043-9be7ee2cadbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}