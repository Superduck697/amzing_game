{
    "id": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7022e080-ee65-41dd-aa27-876c7c61a96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "ae653d12-c3a1-45a3-b30f-b0320ffa8061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7022e080-ee65-41dd-aa27-876c7c61a96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41cd268e-5cf5-4eab-b7c7-b4ee0c4d0c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7022e080-ee65-41dd-aa27-876c7c61a96e",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "a9cfbb85-9787-42a0-9729-cedc1fc32611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "c3938f72-b722-43a6-8e3d-08a9e4fa99dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9cfbb85-9787-42a0-9729-cedc1fc32611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba3d67ed-2ec3-452d-a63f-d44478622d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9cfbb85-9787-42a0-9729-cedc1fc32611",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "9aa308d7-285b-488f-b513-ef3a551cd114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "b123fd09-aff4-476f-9327-b2d3a40c38d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa308d7-285b-488f-b513-ef3a551cd114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9991bb53-acca-4ba4-87e0-0096b889040e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa308d7-285b-488f-b513-ef3a551cd114",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "56f8c821-1547-4538-9394-c4d956dc6083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "1e93cbab-6bcb-4e83-878b-c5ef45acfc80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f8c821-1547-4538-9394-c4d956dc6083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09cee1e9-a746-43ae-8000-3f2de057f144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f8c821-1547-4538-9394-c4d956dc6083",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "6bc4eb19-5d2b-4d2b-ba98-a2531a1469be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "15b85547-d975-42f0-87f6-8407757c3c1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc4eb19-5d2b-4d2b-ba98-a2531a1469be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30cc739a-921a-46c7-9b70-b1d57ef571dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc4eb19-5d2b-4d2b-ba98-a2531a1469be",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "60d789ec-288c-406c-bba8-fc576cb00bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "db609881-c609-417d-b826-8b3441622546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d789ec-288c-406c-bba8-fc576cb00bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdcda267-a00f-47c2-a7dd-1d518d128d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d789ec-288c-406c-bba8-fc576cb00bfd",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "fcf3b5ce-eab7-4a42-8fa4-ead60341772d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "dbd94b56-9543-4061-aa12-82286d7fec6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf3b5ce-eab7-4a42-8fa4-ead60341772d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c805a15c-0aa9-4536-84d9-4490ded90f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf3b5ce-eab7-4a42-8fa4-ead60341772d",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        },
        {
            "id": "740b93a8-9ff7-46c4-9012-654b465d363d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "compositeImage": {
                "id": "5c3df56e-b19f-4b5a-9852-ca829e1f9543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740b93a8-9ff7-46c4-9012-654b465d363d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5472e681-6037-4bbd-bc53-11c3ea6daa0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740b93a8-9ff7-46c4-9012-654b465d363d",
                    "LayerId": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2ad94f2-73e4-4881-b63c-6ea9e9946f3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39df3cc8-e979-4fb7-b8cc-90620ccfea09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}