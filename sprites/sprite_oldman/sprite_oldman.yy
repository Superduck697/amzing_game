{
    "id": "1ceeb422-827b-4463-bbf6-32d31983541b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_oldman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da294c2f-4297-4080-99df-c4f83c6465d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "f865c2d2-12e9-469a-b3c4-06a05bfb9411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da294c2f-4297-4080-99df-c4f83c6465d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39354075-2a12-4bfa-9df4-ef08bce1373c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da294c2f-4297-4080-99df-c4f83c6465d5",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "a4bd87b8-b8c1-4074-813e-f572e1ccc6e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "25908855-df79-41c9-8838-45cc028deb59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4bd87b8-b8c1-4074-813e-f572e1ccc6e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c42b02-4522-4fc8-b7ce-2203e724cf83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4bd87b8-b8c1-4074-813e-f572e1ccc6e2",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "8b07e1fd-de22-43ca-a05e-fd3fecaa6968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "54091fdc-9da2-4b5d-97a3-a4711d56240b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b07e1fd-de22-43ca-a05e-fd3fecaa6968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b470e4-150e-432f-aa1a-ba56f4e40405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b07e1fd-de22-43ca-a05e-fd3fecaa6968",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "f578fa2d-ca9c-4fe6-aebe-3097cf9db6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "c9765b2a-d271-43ef-a611-d035b8e6d0d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f578fa2d-ca9c-4fe6-aebe-3097cf9db6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38836b37-3751-4b03-b174-db84dd01327f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f578fa2d-ca9c-4fe6-aebe-3097cf9db6fe",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "3f02f9d1-2958-487c-ba94-3fc3adfe0d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "53e6e08c-b378-4b74-8daf-e8e2b1d9797e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f02f9d1-2958-487c-ba94-3fc3adfe0d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9876fe8-ec0e-4b54-8161-b789721a1a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f02f9d1-2958-487c-ba94-3fc3adfe0d2f",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "3eb0ec2e-bf41-4523-8da1-d44f53f7be2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "a4897925-0bda-477b-9396-5ed522b753ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eb0ec2e-bf41-4523-8da1-d44f53f7be2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daafef7c-8915-498b-8611-fb465242f19e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eb0ec2e-bf41-4523-8da1-d44f53f7be2c",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "43e25922-5374-46ab-982a-d9cad9e6e305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "4dca77ff-6e85-424c-a818-ea7aff3e24ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e25922-5374-46ab-982a-d9cad9e6e305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f86dc8-d34a-44c0-8069-c4c9331fb2fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e25922-5374-46ab-982a-d9cad9e6e305",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "649fbb2b-9004-455f-944f-f31e3bed01e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "3bb7e97b-e491-4c8c-82f4-00b66b32b3f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "649fbb2b-9004-455f-944f-f31e3bed01e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1656c5-7714-4f46-9310-6c2dd3edc583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "649fbb2b-9004-455f-944f-f31e3bed01e6",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "b6c7179d-d811-4d04-88c4-e45d145dd3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "9bc31568-c9d2-4500-b209-eec2398dcd47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c7179d-d811-4d04-88c4-e45d145dd3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ffed02-65bc-4ff5-8da4-86b18ae3bbbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c7179d-d811-4d04-88c4-e45d145dd3b8",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "fded5a69-99dd-469c-bbff-25bd72aad348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "658b37bf-e1c0-418c-971e-c272ecc92501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fded5a69-99dd-469c-bbff-25bd72aad348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e553c5f-1579-4a42-a9c2-e86fdee61888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fded5a69-99dd-469c-bbff-25bd72aad348",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "5c6510b3-11a7-4daa-85d0-69c51b367fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "132ad339-e1c2-423c-8f8a-50dc03b8cb52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6510b3-11a7-4daa-85d0-69c51b367fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdcc7413-fb06-4a80-9fdf-f6da8ad8a054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6510b3-11a7-4daa-85d0-69c51b367fa5",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "68e8eb66-3d79-40ae-afcf-76003f49ecd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "d8c2fcd8-89df-4516-afa3-c05c6383056e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e8eb66-3d79-40ae-afcf-76003f49ecd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bf12b8-0f58-423c-8658-eed6f2370678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e8eb66-3d79-40ae-afcf-76003f49ecd0",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "4bac43af-2207-4946-b8ce-155d579f0ef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "c9144961-cd81-4a41-acfa-227f0ba9f4a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bac43af-2207-4946-b8ce-155d579f0ef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84ac127-2414-433b-8634-792caccbb51d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bac43af-2207-4946-b8ce-155d579f0ef7",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        },
        {
            "id": "febe2f12-e811-4bc8-9741-13108e1761b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "compositeImage": {
                "id": "ac138625-3bf8-4355-aca6-70ecbc1410bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "febe2f12-e811-4bc8-9741-13108e1761b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0df6eea-9504-4f9e-9641-2c29415a16ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "febe2f12-e811-4bc8-9741-13108e1761b3",
                    "LayerId": "b3465aa0-d215-4443-99d1-66909070e0d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b3465aa0-d215-4443-99d1-66909070e0d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ceeb422-827b-4463-bbf6-32d31983541b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}