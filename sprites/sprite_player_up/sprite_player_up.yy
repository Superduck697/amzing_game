{
    "id": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9fe90a4-cdb5-4094-aa0b-d61eb23f23db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "905faa60-e79b-4d07-8226-9e4ce4281a42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9fe90a4-cdb5-4094-aa0b-d61eb23f23db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071962c3-5ee0-4c06-b7e8-1d155ef68637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9fe90a4-cdb5-4094-aa0b-d61eb23f23db",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "1a525833-8047-46d9-89d2-7bb05d6d2e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "a0d952a1-3902-41e3-9ba5-728399cbf518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a525833-8047-46d9-89d2-7bb05d6d2e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a6833a-4ab1-4334-8ae9-2a46bd043030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a525833-8047-46d9-89d2-7bb05d6d2e0b",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "2ada7b99-6934-45e3-bb97-106eccd1c5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "cebe1ab6-5a52-4d30-a221-65ad9d05e509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ada7b99-6934-45e3-bb97-106eccd1c5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29511efd-c211-46f1-b13c-dce6b8ff327e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ada7b99-6934-45e3-bb97-106eccd1c5ce",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "aee6c603-7411-4f8e-9ede-0ed08951edc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "60840c04-b75d-4ad5-b06f-05e32fc9b4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee6c603-7411-4f8e-9ede-0ed08951edc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e71efa-7f9b-426e-a076-10778d9616f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee6c603-7411-4f8e-9ede-0ed08951edc6",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "344384a4-6074-4677-9738-034a9dea8a04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "3a2a289e-c830-4394-a8d0-841e08d8d520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "344384a4-6074-4677-9738-034a9dea8a04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b9eb64-1903-476c-93b2-6c6ca2231c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "344384a4-6074-4677-9738-034a9dea8a04",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "49db92e6-0fdb-46f8-b045-c5c0a3cedb53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "b46fae51-f343-46f9-8205-c4090a8fb6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49db92e6-0fdb-46f8-b045-c5c0a3cedb53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7a93683-1d7a-44d4-a32f-aa92706b93bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49db92e6-0fdb-46f8-b045-c5c0a3cedb53",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "8eb77cbe-b3da-4811-9916-c30b460a9e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "617c092f-e644-4fe5-a450-5152ba2cf40a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb77cbe-b3da-4811-9916-c30b460a9e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79211473-0ba4-4521-a2a0-bebe00bea26a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb77cbe-b3da-4811-9916-c30b460a9e7d",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        },
        {
            "id": "a80f2343-2c01-429e-93e5-5953bbb2c8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "compositeImage": {
                "id": "dc5caf3a-809c-4a7e-8c64-28587031b3db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80f2343-2c01-429e-93e5-5953bbb2c8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4df51e6-3a9f-45fb-abb1-6097847613a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80f2343-2c01-429e-93e5-5953bbb2c8b7",
                    "LayerId": "d064c0c7-4c73-4a53-b7a9-5862976974e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d064c0c7-4c73-4a53-b7a9-5862976974e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f96eca6-ac1c-443f-8a5a-fb54c2939a85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}