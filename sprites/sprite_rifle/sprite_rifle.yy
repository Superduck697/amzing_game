{
    "id": "3202e9a0-2827-4e46-886b-1707d7855f45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "134dd396-b37b-47ee-b94d-4d667a4d9c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3202e9a0-2827-4e46-886b-1707d7855f45",
            "compositeImage": {
                "id": "03dfad57-0d33-4835-a560-22d4d981b168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134dd396-b37b-47ee-b94d-4d667a4d9c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7d3388-d0bd-4083-99e9-2b1c013ee180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134dd396-b37b-47ee-b94d-4d667a4d9c2a",
                    "LayerId": "7b82f17a-8ee6-4c2f-9032-cb9748e1fbd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b82f17a-8ee6-4c2f-9032-cb9748e1fbd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3202e9a0-2827-4e46-886b-1707d7855f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}