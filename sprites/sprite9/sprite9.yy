{
    "id": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c740bd64-e7c4-44b2-b924-158e3d7d3d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "d6158f1f-c0ed-4b2b-988e-d7ba902005c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c740bd64-e7c4-44b2-b924-158e3d7d3d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4273d5ca-5c14-4961-80a7-df5f6d54f35b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c740bd64-e7c4-44b2-b924-158e3d7d3d6c",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "d06c6007-eb5a-4847-8b12-6acc92bb691d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "3d4bac41-1ffb-41ce-b54c-0b542c8f2da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06c6007-eb5a-4847-8b12-6acc92bb691d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5df32bee-79ac-4307-b807-b69bcbefbb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06c6007-eb5a-4847-8b12-6acc92bb691d",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "6d279927-b676-45a5-9a11-9fcad4814c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "630ef8e5-c45d-430f-bc76-7a104c24630f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d279927-b676-45a5-9a11-9fcad4814c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7776ad06-3885-4ae5-891e-029ca2c45513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d279927-b676-45a5-9a11-9fcad4814c45",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "a85d8fff-3a22-4284-9dba-3266476ffb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "63a36fa9-bda2-4bdf-a116-9482cfa209d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85d8fff-3a22-4284-9dba-3266476ffb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a45662-a74c-431e-b1da-307a3b55b884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85d8fff-3a22-4284-9dba-3266476ffb3d",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "41075f78-3462-4213-89ed-4a1352bd614c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "e43269e6-1921-4629-820d-157b8fb197c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41075f78-3462-4213-89ed-4a1352bd614c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c74260-9951-4b27-88b2-2c5185a9d855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41075f78-3462-4213-89ed-4a1352bd614c",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "585f134e-df1c-4075-9b82-73ce94973e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "258d262b-fb28-407f-a986-cb7fb5e297c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585f134e-df1c-4075-9b82-73ce94973e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44dbd669-b27b-4158-9898-b53db59b50dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585f134e-df1c-4075-9b82-73ce94973e14",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        },
        {
            "id": "3dea09bc-35f4-4245-81c1-42c04842eb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "compositeImage": {
                "id": "b9a35531-4db3-42c0-9051-1e89e4ab7892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dea09bc-35f4-4245-81c1-42c04842eb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26473272-b392-4f81-8fe7-1d554d87b53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dea09bc-35f4-4245-81c1-42c04842eb3d",
                    "LayerId": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ffed1c5-db4e-4cd9-b031-5188d94c7bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1f92a87-94ac-4144-ab16-aee3851d0d51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}