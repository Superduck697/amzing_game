{
    "id": "0050696e-769f-46eb-8e8d-72d130586cd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d455815-6874-4b76-9f5a-fddc8bf02080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0050696e-769f-46eb-8e8d-72d130586cd2",
            "compositeImage": {
                "id": "1a0db09b-11b0-4a06-a6b8-50c6d38dde8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d455815-6874-4b76-9f5a-fddc8bf02080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c31c52-52a4-4bd2-94a8-fd620f1373f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d455815-6874-4b76-9f5a-fddc8bf02080",
                    "LayerId": "af9e20f6-d57f-44ad-83a0-da62d0e18ad8"
                }
            ]
        },
        {
            "id": "5154f66f-8dec-40e7-b249-f155d8231987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0050696e-769f-46eb-8e8d-72d130586cd2",
            "compositeImage": {
                "id": "716dead1-1f18-41e7-8d9c-26d15ffdb3b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5154f66f-8dec-40e7-b249-f155d8231987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "954c6371-2498-449c-8b5e-709e3ba478db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5154f66f-8dec-40e7-b249-f155d8231987",
                    "LayerId": "af9e20f6-d57f-44ad-83a0-da62d0e18ad8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af9e20f6-d57f-44ad-83a0-da62d0e18ad8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0050696e-769f-46eb-8e8d-72d130586cd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}