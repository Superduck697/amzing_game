{
    "id": "6d0507fb-1011-4a22-a342-5575eb53a977",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_switchdoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35ec697d-7b9e-48ab-bdee-4c5c51720764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d0507fb-1011-4a22-a342-5575eb53a977",
            "compositeImage": {
                "id": "82d5e1e9-12a9-4297-8796-b31363a6041f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ec697d-7b9e-48ab-bdee-4c5c51720764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8136f5ac-3ad4-4875-b35c-c7a39e49b3a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ec697d-7b9e-48ab-bdee-4c5c51720764",
                    "LayerId": "7c97fe58-2aa6-4387-87e0-0a68cf82a392"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c97fe58-2aa6-4387-87e0-0a68cf82a392",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d0507fb-1011-4a22-a342-5575eb53a977",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}