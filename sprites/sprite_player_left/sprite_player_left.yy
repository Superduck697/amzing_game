{
    "id": "40d61877-a306-4a64-abab-f0455cd6a0fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63b4bb55-95f2-485a-943c-302d0f849ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "cce0ee35-6888-456b-b212-2ecd4c658225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b4bb55-95f2-485a-943c-302d0f849ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b03e11c-2d43-40d1-8474-ba5144691b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b4bb55-95f2-485a-943c-302d0f849ac2",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "21697f90-27d2-4665-9f3f-2a7131ca68f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "b722b110-9d45-440e-b163-66a0271b2407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21697f90-27d2-4665-9f3f-2a7131ca68f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8cc9c60-a8b0-4d51-b2cb-3d5e93f6fd61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21697f90-27d2-4665-9f3f-2a7131ca68f4",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "0d96f3a9-4f59-4ed1-ae80-7cb4b13a5da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "55f2b4ab-c143-49c9-8c06-4d5d7478908f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d96f3a9-4f59-4ed1-ae80-7cb4b13a5da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5fbdaf-259b-4eeb-a895-a54f8163ab67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d96f3a9-4f59-4ed1-ae80-7cb4b13a5da5",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "388d4a90-3f7b-4a0b-8c20-45cb81509f9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "229f75a0-fb33-48db-9ee9-b4062ad0cce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388d4a90-3f7b-4a0b-8c20-45cb81509f9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf9c6d0-6472-4f4b-91e6-7f5ec7ebf064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388d4a90-3f7b-4a0b-8c20-45cb81509f9a",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "dc6cc54f-a67d-439e-a00c-385186fa88f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "b424b926-4c58-4869-af0e-2b64ee6a9d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc6cc54f-a67d-439e-a00c-385186fa88f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a66e31-52fc-44db-9333-6dab0a7b9f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc6cc54f-a67d-439e-a00c-385186fa88f2",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "0c3be4fa-7f38-4647-80b7-61ba16f019a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "fe06a88d-5b4c-47ef-924b-b1972fe2d76f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3be4fa-7f38-4647-80b7-61ba16f019a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159977f5-6ba9-4481-8a83-bc627c1ce80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3be4fa-7f38-4647-80b7-61ba16f019a0",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "48557fe3-abf0-4ad5-bb38-28b949626840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "3f10f658-f6cf-4597-b35f-fe33b6278392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48557fe3-abf0-4ad5-bb38-28b949626840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382b3fe6-29a0-43e4-a7b4-112903795a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48557fe3-abf0-4ad5-bb38-28b949626840",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        },
        {
            "id": "e0fc57bf-0f6b-4221-b173-5bda31a79b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "compositeImage": {
                "id": "ec922d55-b695-41b2-a071-12d90cacf819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fc57bf-0f6b-4221-b173-5bda31a79b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6edd513-8e7d-48f3-a035-367557c8a35d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fc57bf-0f6b-4221-b173-5bda31a79b13",
                    "LayerId": "373007ce-71df-4e83-b28b-dc63c0171121"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "373007ce-71df-4e83-b28b-dc63c0171121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40d61877-a306-4a64-abab-f0455cd6a0fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}