{
    "id": "d81f21df-5e18-4330-8c4c-80b90eb99afa",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_1A",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "53fcf2b5-da8e-4e7d-8fd9-741fec9d870d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 64,
            "speed": 100
        },
        {
            "id": "9e6e3898-900b-4301-86a7-67a0d67f6619",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "00478f21-4af8-4067-a90e-521d1cdd0726",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 224,
            "speed": 100
        },
        {
            "id": "f62ae614-f63b-4f52-af34-8a38abe5d020",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 224,
            "speed": 100
        },
        {
            "id": "ea914465-144a-488a-9e99-742acb411fac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 160,
            "speed": 100
        },
        {
            "id": "a7fa21ff-5aff-4d23-ac42-f36d8247130f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 160,
            "speed": 100
        },
        {
            "id": "ce9ecf86-2bce-47cf-958a-89df78f0d82f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 64,
            "speed": 100
        },
        {
            "id": "bf51d4da-4391-4a88-b992-e20d90829f99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 64,
            "speed": 100
        },
        {
            "id": "216738d1-bb9a-47c3-a9b8-0ee6e0c28da0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 192,
            "speed": 100
        },
        {
            "id": "2fdbd50a-2db9-4246-8647-4dda676458d2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 64,
            "speed": 100
        },
        {
            "id": "24210a87-aaba-4b77-acc6-a35431a1ae91",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 64,
            "speed": 100
        },
        {
            "id": "613bf9dd-0c32-4d32-82dc-1773b476d5a3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 288,
            "speed": 100
        },
        {
            "id": "c68394ed-412f-4752-a765-d8506c82c69f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 352,
            "speed": 100
        },
        {
            "id": "e7c07c40-c1fd-465a-b290-eddb4787a4a8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 416,
            "speed": 100
        },
        {
            "id": "e3896946-22c2-4981-8290-3e251fa91c43",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 224,
            "speed": 100
        },
        {
            "id": "8fa54aae-eb4b-42ad-8744-458b106e0ec9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 288,
            "speed": 100
        },
        {
            "id": "650e34e9-3649-415f-9fb3-4bed952e24dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 352,
            "speed": 100
        },
        {
            "id": "fa187301-b9ff-4025-8e73-51be5dbb37e7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 480,
            "speed": 100
        },
        {
            "id": "eda4d508-9689-4f17-9656-4d92ff548681",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 384,
            "speed": 100
        },
        {
            "id": "ba549291-3fe9-4d2e-9ec6-36d499db2dce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 512,
            "speed": 100
        },
        {
            "id": "3f959a8e-ba49-43bc-8540-9cf00017aa7f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 608,
            "speed": 100
        },
        {
            "id": "77c7aed5-1663-45e4-bad0-3180a0d8ff93",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 672,
            "speed": 100
        },
        {
            "id": "e4fdbf47-825a-4c56-a380-00fc79eec02a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 672,
            "speed": 100
        },
        {
            "id": "e178f437-f9c3-4f65-86d0-3d2e12dd065c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 672,
            "speed": 100
        },
        {
            "id": "2c514863-f8a5-45d1-945f-8185576b3d32",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 672,
            "speed": 100
        },
        {
            "id": "770f9a05-5150-4603-b6eb-9bfb2138c477",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 672,
            "speed": 100
        },
        {
            "id": "ccde0b9b-9d57-4aef-a43e-036763aeb3c8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 672,
            "speed": 100
        },
        {
            "id": "20dc7d12-54bb-46dc-8a1a-a48f8b172905",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 672,
            "speed": 100
        },
        {
            "id": "3d132490-1325-4e87-bd5c-e015f7167ccd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 608,
            "speed": 100
        },
        {
            "id": "28292480-17d4-44f6-9811-c1e4b4306c0b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 576,
            "speed": 100
        },
        {
            "id": "9de42215-8e6d-4fa6-a610-7fd98362189e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 640,
            "speed": 100
        },
        {
            "id": "855ad692-37b3-435c-b8fc-ffcd39b6a628",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 608,
            "speed": 100
        },
        {
            "id": "3424cb04-fc0a-490f-b7c0-d9c2a63203bc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 448,
            "speed": 100
        },
        {
            "id": "009ea7b3-b113-4d18-bbc6-0a83e631c4e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 480,
            "speed": 100
        },
        {
            "id": "990ada15-b461-4cf4-b16a-d0a5592de9dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 416,
            "speed": 100
        },
        {
            "id": "f0e1099a-c48a-48ca-a2aa-54046f95b68c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 320,
            "speed": 100
        },
        {
            "id": "6c0fb5ee-02f9-473f-96d2-30115ded6b59",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 288,
            "speed": 100
        },
        {
            "id": "f0577dc1-1658-431b-b68c-90ba52234b37",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 352,
            "speed": 100
        },
        {
            "id": "57d4caa4-d573-411c-8f32-cc3b9295127a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 512,
            "speed": 100
        },
        {
            "id": "0273193b-93a5-421a-ae37-53110e00e02e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 512,
            "speed": 100
        },
        {
            "id": "aef88a62-19cd-4a26-9244-183d7f131613",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "e3fae297-3351-42a2-8e49-0881d60f98e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 288,
            "speed": 100
        },
        {
            "id": "872c2c7a-b57a-4b28-b23b-067f67b42d15",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 288,
            "speed": 100
        },
        {
            "id": "081c41b7-2f09-471b-8f5f-fe90cf2300cd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 256,
            "speed": 100
        },
        {
            "id": "87726ca3-2ac8-4ca3-95c4-efdb5a1e69df",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 192,
            "speed": 100
        },
        {
            "id": "72acfc3c-b06f-4250-ba37-36a043eb6ab0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 256,
            "speed": 100
        },
        {
            "id": "f04058a8-228f-40e7-baea-6bd712f84f7d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 192,
            "speed": 100
        },
        {
            "id": "b60aab88-3dd5-4040-b34e-7f13b7913958",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 160,
            "speed": 100
        },
        {
            "id": "0f072a6c-03c5-403f-9310-619c2a44e924",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 160,
            "speed": 100
        },
        {
            "id": "206d5d3c-9fef-4d98-b265-ffa744181fed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 224,
            "speed": 100
        },
        {
            "id": "53ed4ad4-8c95-4389-a8e1-1ad304ece614",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 352,
            "speed": 100
        },
        {
            "id": "1d391b7a-4ac3-405a-b11b-d3854a462fb8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 352,
            "speed": 100
        },
        {
            "id": "063a8c37-b2b4-4c2e-a6a2-beb3297af050",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 640,
            "speed": 100
        },
        {
            "id": "5a15d6e7-4800-49b2-9b1b-68aa53e4c129",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 512,
            "speed": 100
        },
        {
            "id": "f8c49e4a-4e53-4c61-9235-86d35df8aa4e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 608,
            "speed": 100
        },
        {
            "id": "3e732cb1-9be4-41b2-988a-d508d5f232a8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 512,
            "speed": 100
        },
        {
            "id": "9aa758f7-592f-4d79-91e4-ac18a8be5012",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 512,
            "speed": 100
        },
        {
            "id": "da8e25c2-b157-4035-8b53-a987cfafab67",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 512,
            "speed": 100
        },
        {
            "id": "2a774f44-61a4-4744-b5fb-89fbcc495850",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 288,
            "speed": 100
        },
        {
            "id": "523ccff7-c4a6-4983-bdce-4424b27bab83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 256,
            "speed": 100
        },
        {
            "id": "1c47e0ce-2331-41a2-82be-61682829f7f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 608,
            "speed": 100
        },
        {
            "id": "542f3310-38a4-447e-9acc-beed395fe34e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 608,
            "speed": 100
        },
        {
            "id": "d44eca09-4aaa-4f5a-b9a6-2f037c651014",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 704,
            "speed": 100
        },
        {
            "id": "2a66376f-6ca5-412d-8405-4566acc75cc1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 704,
            "speed": 100
        },
        {
            "id": "bdcf8715-5244-4cc3-9b2e-6665f6ae7c4f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 608,
            "speed": 100
        },
        {
            "id": "f6b75f59-53a8-4965-8462-1541940321b3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 608,
            "speed": 100
        },
        {
            "id": "3c635ff4-70a2-4c59-b6e9-e05a02ec96ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 608,
            "speed": 100
        },
        {
            "id": "086475e9-45d8-431f-96b8-eb8c256eef57",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 704,
            "speed": 100
        },
        {
            "id": "1a8cf397-57ee-44bc-806c-f0cdd0e18f9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 704,
            "speed": 100
        },
        {
            "id": "112fb53a-a362-4713-902c-06a06e5b7f26",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "0734b9f7-b893-4509-a87c-5c847eef2030",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 320,
            "speed": 100
        },
        {
            "id": "b60c7ead-18b3-497a-9656-4a1f39d40785",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 96,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}