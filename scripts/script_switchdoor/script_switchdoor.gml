/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 42E4B399
/// @DnDArgument : "code" "if (object_switch.doorsAreClear == true)$(13_10){$(13_10)	image_index = 0;$(13_10)	audio_play_sound(sound_door, 50, false);$(13_10)	isActivated = false;$(13_10)	for (var i = 0; i < array_length_1d(switchdoors); i ++) {$(13_10)		switchdoors[i].y = switchdoors[i].ystart;$(13_10)	}$(13_10)}$(13_10)else$(13_10){$(13_10)	alarm_set(1,5);$(13_10)}$(13_10)$(13_10)"
if (object_switch.doorsAreClear == true)
{
	image_index = 0;
	audio_play_sound(sound_door, 50, false);
	isActivated = false;
	for (var i = 0; i < array_length_1d(switchdoors); i ++) {
		switchdoors[i].y = switchdoors[i].ystart;
	}
}
else
{
	alarm_set(1,5);
}